﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    private jumpState _jumpState;
    private jumpDirection _jumpDirection;
    private Rigidbody _rb;
    private float _rippleRadius = 300f;
    
    private GameController _gameController;
    private Model _model;
    
    private float _rippleDist;
    // private Timer _rippleTimer;

    public float rippleSpeed = 2f;
    public float superJumpThreshold = 9f;
    public float floatDuration = 1f;
    public int superjumpEnergyThreshold = 15;
    public float jumpImpulse = 5;
    public float superjumpImpulse = 10;
    public GameObject text;

    private Vector3 _originalPosition;
    private float _velY = 0;
    private float _accelY = 0;
    // private float _vellDecay;

    public float jumpVel = 5f;
    public float jumpAccel = 0.98f;
    public float jumpFloat = 0.01f;
    public float maxVel = 0.8f;

    public AudioSource audioJump;
    public AudioSource audioLand;

    public const string PLANE_NAME = "Plane";

    // Use this for initialization
    void Start () {
        _gameController = GameController.instance;
        _model = Model.instance;

        _rb = GetComponent<Rigidbody>();

        //AudioSource[] tAudioSources = GetComponents<AudioSource>();
        //audioJump = tAudioSources[0];
        //audioLand = tAudioSources[1];
        //Debug.Log(string.Format("player jumps: {0}, {1}",audioJump, audioLand));
        //audioJump = GetComponent<AudioSource>();
        audioJump = GetComponents<AudioSource>()[0];
        audioLand = GetComponents<AudioSource>()[1];
        // Debug.Log("audiojump: " + audioJump);

        // Debug.Log("auds: " + GetComponents<AudioSource>().Length);

        _model.RegisterPlayer(gameObject);

        _originalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        // Debug.Log(string.Format("head game object: {0}, controller left: {1}", GameObject.Find( "Camera (head)" ), GameObject.Find("Controller (left)")) );
    }

    // Update is called once per frame
    void Update () {
		//if( IsJumping() && _jumpDirection == jumpDirection.up )
  //      {
  //          // reached peak
  //          if( _rb.velocity.y < 0 )
  //          {
  //              _jumpDirection = jumpDirection.down; 
  //          }

  //          // superjump floats at peak
  //          if( _jumpState == jumpState.superjump)
  //          {
  //              Physics.gravity = new Vector3(0, 0, 0);
  //              StartCoroutine(DelayFall());
  //          }
  //      }

        // detect superjump
        // ug.Log(string.Format("vel: {0}", _rb.velocity.y));
        //if (!IsJumping() && _rb.velocity.y > 1f)
        //{
        //    // empty
        //}

        // text.GetComponent<TextMesh>().text = _model.jumpEnergy.ToString();

        if (IsJumping())
        {
            _velY *= _accelY;

            if (_velY < 0.001 && _velY > 0)
            {
                _velY *= -1f;
                _accelY = 1f + (1f - jumpAccel);
            }

            _velY = Mathf.Clamp(_velY, -maxVel, maxVel);

            transform.position += new Vector3(0, _velY, 0);

            if (transform.position.y < _originalPosition.y)
            {
                transform.position = _originalPosition;

                 _jumpState = jumpState.idle;
            }
        }
    }

    IEnumerator DelayFall()
    {
        yield return new WaitForSeconds(floatDuration);

        Debug.Log("delayfall complete");
        Physics.gravity = new Vector3(0, -9.8f, 0);
    }

    void OnMouseDown()
    {
        Jump();
    }

    public void Jump()
    {
        // -9.8 is default
        //float tGravity;// = Random.Range(-7f, -20f);
        //if( _model.jumpEnergy >= superJumpThreshold )
        //{
        //    tGravity = -superjumpImpulse;
        //}
        //else
        //{
        //    tGravity = -jumpImpulse;
        //}
        //Debug.Log(string.Format("gravity: {0}", tGravity));

        //Physics.gravity = new Vector3(0, tGravity, 0);
        //_rb.AddForce(transform.up * 10f, ForceMode.Impulse);

        //_jumpDirection = jumpDirection.up;
        _jumpState = jumpState.jump;
    }

    void SuperJump()
    {
        _jumpState = jumpState.superjump;
    }

    void OnCollisionEnter(Collision other)
    {
        // Debug.Log("landed on: " + other.gameObject.name );

        if ( other.gameObject.name == PLANE_NAME )
        { 
            // when a landing happens after a normal jump only
            if( _jumpState == jumpState.jump )
            { 
                BeginRipple();
            } else 
            {
                // empty
            }

            _jumpState = jumpState.idle;
        }
    }

    public void JumpAll()
    {
        foreach (GameObject go in _model.plants)
        {
            PlantVC tPlantVC = go.GetComponent<PlantVC>();
            tPlantVC.jump();
        }
    }

    public void BeginRipple()
    {
        _rippleDist = 3f;

        StartCoroutine(RippleStep());
    }

    public IEnumerator RippleStep()
    {
        Debug.Log("rippleStep");
        
        
        while(_rippleDist < _rippleRadius )
        {
            _rippleDist += rippleSpeed;
            //Debug.Log("ripple dist: " + _rippleDist);
                
            foreach (GameObject tPlant in _model.plants)
            {
                float tDist = (tPlant.transform.position - _originalPosition).magnitude;
                PlantVC tPlantVC = tPlant.GetComponent<PlantVC>();
                if (_rippleDist > tDist && !tPlantVC.IsJumping && !tPlantVC.preventAdditionalJumps )
                {
                    tPlantVC.jump();
                    tPlantVC.preventAdditionalJumps = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }

        foreach (GameObject tPlant in _model.plants)
        {
            tPlant.GetComponent<PlantVC>().preventAdditionalJumps = false;
        }

    }


    public bool IsJumping()
    {
        return (_jumpState == jumpState.jump || _jumpState == jumpState.superjump);
    }

}

public enum jumpState
{
    idle,
    jump,
    superjump
}

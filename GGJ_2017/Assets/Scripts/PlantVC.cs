﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioSource))]
public class PlantVC : MonoBehaviour {

    private bool _isRevealed = false;
    private float _hideTime;
    private float _triggerEnergyAccumulated;
    private float _triggerPowerRequired;
    private Model _model;
    private bool _isJumping = false;
    private jumpDirection _jumpDirection;
    private Rigidbody _rb;

    private Vector3 _originalPosition;
    private float _velY = 0;
    private float _accelY = 0;
    // private AudioSource _audio;
    // private float _vellDecay;

    public float jumpVel = 1f;
    public float jumpAccel = 0.9f;
    public float jumpFloat = 0.01f;
    public float maxVel = 0.8f;
    public bool preventAdditionalJumps = false;


    public const string TREES_FILL = "Trees";
    public const string GRASS_FILL = "Grass";
    public const string PLANTS_FILL = "Plants";

    public PlantType type;
    public int jumpEnergyTypeA = 1;
    public int jumpEnergyTypeB = 3;
    public int jumpEnergyTypeC = 7;

    public float jumpImpulse = 0.2f;

    public bool IsJumping {
        get { return _isJumping; }
        set { _isJumping = value; }
    }
    
    // Use this for initialization
    void Start () {
        _model = Model.instance;
        _model.RegisterPlant(gameObject);

        _rb = GetComponent<Rigidbody>();

        //_audio = GetComponent<AudioSource>();
        //_audio.Play();
        //Debug.Log("plant audio: " + _audio);
    

        switch (gameObject.transform.parent.gameObject.name)
        {
                case TREES_FILL:
                case GRASS_FILL:
                case PLANTS_FILL:
                    Vector3 tAdjustPosition = new Vector3(Random.Range(-3f, 3f), 0, Random.Range(-7f, 7f));
                    transform.position += tAdjustPosition;

                    float tAdjustRotation = Random.Range(-180f, 180f);
                    transform.Rotate(Vector3.up, tAdjustRotation);
                break;
            default:
                break;
        }

        _originalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        // jump();
    }

    void Awake()
    {
       // empty
    }

    // Update is called once per frame
    void Update () {
        // transform.position += new Vector3(0, 0.2f, 0);
        if(_isJumping)
        {
            _velY *= _accelY;
           
            if( _velY < jumpFloat && _velY > 0 )
            {
                _velY *= -1f;
                _accelY = 1f + ( 1f - jumpAccel );
            }

            _velY = Mathf.Clamp(_velY, -maxVel, maxVel);

            transform.position += new Vector3(0, _velY, 0);
            
            if( transform.position.y < _originalPosition.y )
            {
                transform.position = _originalPosition;

                _isJumping = false;
            }
        }
	}

    public void jump()
    {
        if (_isJumping)
            return;

        // _rb.isKinematic = false;
        
        // addJumpEnergy();

        // -9.8 is default
        //  float tGravity = -9.8f;
        // Debug.Log(string.Format("plant jump"));

        // Physics.gravity = new Vector3(0, tGravity, 0);
        //Vector3 tWorldUp = transform.InverseTransformDirection(Vector3.up);
        //_rb.AddForce(tWorldUp * jumpImpulse, ForceMode.Impulse);

        //_jumpDirection = jumpDirection.up;
        _isJumping = true;

        _velY = jumpVel;
        _accelY = jumpAccel;

        //_audio.Play();
    }

    void OnCollisionEnter(Collision other)
    {
       // empty
    }

    void addJumpEnergy()
    {
        switch( type )
        {
            case PlantType.a:
                _model.jumpEnergy += jumpEnergyTypeA;
                    break;
            case PlantType.b:
                _model.jumpEnergy += jumpEnergyTypeB;
                break;
            case PlantType.c:
                _model.jumpEnergy += jumpEnergyTypeC;
                break;
            default:
                break;
        }   
    }
}

public enum PlantType
{
    a, 
    b, 
    c
}
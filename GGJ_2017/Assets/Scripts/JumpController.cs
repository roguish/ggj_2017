﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(SteamVR_TrackedObject))]
public class JumpController : MonoBehaviour
{
    private SteamVR_TrackedObject _trackedObj;
    private SteamVR_Controller.Device _device;
    private float _prevPosY = 0;
    private float _prevPosYDelta = 0;
    private float _maxDeltaPosY = 0;
    private bool _prevFrameWasAboveThreshold = false;

    public float jumpTriggerYVelocity = 0.02f;

    // Use this for initialization
    void Start()
    {
        Debug.Log("ControllerTEST start");

        _trackedObj = GetComponent<SteamVR_TrackedObject>();
        _device = SteamVR_Controller.Input((int)_trackedObj.index);

        _prevPosY = _device.transform.pos.y;
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log("ControllerTEST update");

        float tPosY = _device.transform.pos.y;
        float tPosYDelta = tPosY - _prevPosY;
        if( tPosYDelta > _maxDeltaPosY )
        {
            _maxDeltaPosY = tPosYDelta;
        }

        // wait for peak
        // previous was greater than threshold and this one is lower
        if (_prevFrameWasAboveThreshold && _prevPosYDelta >= tPosYDelta )
        {
            // previous was above threshold and we've slowed, so peak was reached
            // Debug.Log("JUMP sending: " + tPosYDelta);
            Messenger.Broadcast(JumpCamera.JUMP_TRIGGERED, _prevPosYDelta);
        }

        if (tPosYDelta > jumpTriggerYVelocity)
        {
            _prevFrameWasAboveThreshold = true;
        }
        else
        {
            _prevFrameWasAboveThreshold = false;
        }

        _prevPosY = tPosY;
        _prevPosYDelta = tPosYDelta;
        // Debug.Log(string.Format("_maxDeltaPosY: {0},                                                               {1}", _maxDeltaPosY, tPosYDelta));
        // Debug.Log(string.Format("tPosYDelta: {0}", tPosYDelta));
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpCamera : MonoBehaviour
{
    private Vector3 _floatVelocity = new Vector3(0, 0.5f, 0);
    private Vector3 _jumpImpulse = new Vector3(0f, 20f, 0);
    private Rigidbody _rb;
    private bool _isJumping = false;
    private int _moveDir = 1;
    private Vector3 _moveVect = new Vector3(0, 0.5f,0);
    private Player _player;

    public static string JUMP_TRIGGERED = "JumpTriggered";

    public float rippleSpeed = 1f;
    public float superJumpThreshold = 9f;
    public float floatDuration = 1f;
    public int superjumpEnergyThreshold = 15;
    public float jumpImpulse = 5;
    public float superjumpImpulse = 10;
    public GameObject text;

    private Vector3 _originalPosition;
    private float _velY = 0;
    private float _accelY = 0;
    private float _decelY = 0;

    // private float _vellDecay;
    public float jumpVel = 50f;
    public float jumpAccel = 0.94f;
    public float jumpFloat = 0.03f;
    public float maxVel = 4f;

    // Use this for initialization
    void Start()
    {
        // _rb = GetComponent<Rigidbody>();
        _player = GameObject.Find("Player").GetComponent<Player>();
        Debug.Log(string.Format("Jump Camera, rb: {0}", _rb));

        _originalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        Messenger.AddListener<float>(JUMP_TRIGGERED, OnJumpTriggered);
    }

    // Update is called once per frame
    void Update()
    {
        //int test = 0;
        //// Debug.Log(string.Format("JUMP vel: {0}, vel: {1}", _rb.velocity.y, transform.position.y));
        //if (_rb.velocity.y > 0 && !_isJumping)
        //{
        //    //_isJumping = true;
        //    //_rb.AddForce(_jumpImpulse, ForceMode.Impulse);

        //    Debug.Log(string.Format("JUMP vel: {0}", _rb.velocity.y));
        //}

        //transform.position += _moveVect * _moveDir;
        //if (transform.position.y > 150f || transform.position.y < 1f)
        //{
        //    _moveDir *= -1;
        //    transform.position += _moveVect * 2f * _moveDir;
        //}

        if (_isJumping)
        {
            _velY *= _accelY;

            // turn downwards at the same accel used to rise
            if (_velY < jumpFloat && _velY > 0)
            {
                _velY *= -1f;
                _accelY = 1f + (1f - _decelY);
            }

            _velY = Mathf.Clamp(_velY, -maxVel, maxVel);

            transform.position += new Vector3(0, _velY, 0);

            if (transform.position.y < _originalPosition.y)
            {
                transform.position = _originalPosition;

                _isJumping = false;

                _player.BeginRipple();

                _player.audioLand.Play();
            }
        }
    }

    void OnJumpTriggered( float pStrength )
    {
        Debug.Log( string.Format("OnJumpTriggered received at: {0}, {1}, {2}", Time.time, _isJumping, _rb ) );

        Jump(pStrength);
    }

    void Jump(float pStrength)
    {
        if (_isJumping)
            return;

        // transform.position = new Vector3(0, 50f, 0);
        // _player.JumpAll();

        _isJumping = true;

        _player.audioJump.Play();
        
        // pStrength .02 - .04
        _velY = jumpVel + 8 * ( pStrength * 1000 );

        // more floaty accel with stronger jump
        float tAccel = Mathf.Min( 0.98f, jumpAccel + pStrength);
        _decelY = tAccel;
        _accelY = tAccel;
        Debug.Log(string.Format("jump data: {0}, {1}, {2}", pStrength, tAccel, _velY));
        // _player.BeginRipple();
    }


}


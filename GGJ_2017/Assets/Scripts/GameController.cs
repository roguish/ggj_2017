﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Timers;
using System;

public class GameController {

    private static GameController _instance;
    private Model _model;
    

    private GameController()
    {
        _model = Model.instance;
        // _rippleTimer = new Timer();

        // UnityThread.initUnityThread();
    }

    public static GameController instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameController();
            }
            return _instance;
        }

    }


}

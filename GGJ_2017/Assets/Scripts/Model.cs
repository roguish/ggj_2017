﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model {
    public float jumpEnergy = 0;

    public List<GameObject> plants;
    public GameObject player;
        
    private static Model _instance;

    public static Model instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new Model();
            }
            return _instance;
        }

    }

    private Model() {
        plants = new List<GameObject>();
	}
	
    public void RegisterPlant( GameObject pPlant )
    {
        plants.Add(pPlant);
        // Debug.Log(string.Format("registered a plant: {0}. Now there are {1}", pPlant.name, plants.Count));
    }

    public void RegisterPlayer( GameObject pPlayer )
    {
        player = pPlayer;
    }
}

enum jumpDirection
{
    up,
    down
};

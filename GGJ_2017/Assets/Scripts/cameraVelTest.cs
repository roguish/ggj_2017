﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class cameraVelTest : MonoBehaviour {
    private SteamVR_TrackedObject _trackedObj;
    private SteamVR_Controller.Device _device;

    // Use this for initialization
    void Start () {
        _trackedObj = GetComponent<SteamVR_TrackedObject>();
        Debug.Log("CamerVelTest TEST start");
        
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log("CamerVelTest TEST update");

        _device = SteamVR_Controller.Input((int)_trackedObj.index);
        Debug.Log(string.Format("CamerVelTest pos.y: {0}", _device.transform.pos.y));
	}
}
